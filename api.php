<?php

header('Content-Type:application/json;charset=utf-8');
//error_reporting(0);

const CLASSES = __DIR__ . DIRECTORY_SEPARATOR . 'Classes' . DIRECTORY_SEPARATOR;
require_once CLASSES . 'Curl.php';
require_once CLASSES . 'Redis.php';
require_once CLASSES . 'Authcode.php';
require_once CLASSES . 'Verify.php';
require_once CLASSES . 'Lanzou.php';
require_once CLASSES . 'common.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'config.php';

use Classes\Lanzou;

extract(array_filter($_REQUEST, function ($v) {
	return is_array($v) ? !empty($v) : $v !== null && trim($v) !== '';
}));

/** @var $conf $conf */
if (isset($c) && $c === 'verify') {
	$verify_conf = $conf['verify'];
	\Classes\Verify::config($verify_conf)->create();
	exit();
}

if (!isset($id)) $id = '-1'; // 根目录

Lanzou::config($conf);

$lz = new Lanzou($id, isset($pwd) ? $pwd : '');
if (isset($c))
	switch ($c) {
		case 'd':
			$ret = $lz->parseUrlByShareId($id, isset($pwd) ? $pwd : '');
			break;
		case 'list':
			$ret = $lz->parseList(isset($page) ? $page : 1);
			break;
		case 'upload':
			$ret = $lz->upload($_FILES);
			break;
		case 'rename':
			if (!isset($name)) $ret = msg(-1, '文件名不能为空');
			else $ret = $lz->rename($name);
			break;
		case 'folder':
			if (!isset($name)) $ret = msg(-1, '文件夹名称不能为空');
			else $ret = $lz->createFolder($name);
			break;
		case 'move':
			if (!isset($file_id)) $ret = msg(-1, '文件id不能为空');
			else $ret = $lz->moveFile($file_id);
			break;
		case 'delete':
			if (!isset($file_id) && !isset($folder_id)) $ret = msg(-1, '文件(夹)id不能为空');
			else $ret = $lz->delete(['file_id' => @$file_id, 'folder_id' => @$folder_id]);
			break;
		case 'login':
			if (!isset($code))
				$ret = msg(-1, '图形验证码不能为空');
			else if (true !== $check = \Classes\Verify::check($code))
				$ret = msg(-2, $check);
			else if (!isset($pass))
				$ret = msg(-3, '管理员密码不能为空');
			else
				$ret = $lz->login($pass);
			break;
		case 'logout':
			$ret = $lz->logout();
			break;
		case 'pwd':
			$ret = $lz->setPwd();
			break;
		default:
			$ret = msg(-3, '未知操作');
	}
else if (isset($name) && $name !== '')
	$ret = $lz->parseList(isset($page) ? $page : 1, $name);
else
	$ret = $lz->parseUrl();

exit(arr2json($ret));