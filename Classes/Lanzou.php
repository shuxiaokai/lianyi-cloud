<?php

namespace Classes;

use Classes\Curl;
use Classes\Redis;
use Classes\Authcode;

class Lanzou {
	const API        = 'https://pc.woozooo.com/doupload.php';
	const API_UPLOAD = 'https://pc.woozooo.com/fileup.php';

	const allowedExts = [
		'7z', 'accdb', 'apk', 'appimage', 'azw', 'azw3', 'bat', 'bdi', 'bds', 'cad', 'ce', 'cetrainer', 'conf', 'cpk',
		'crx', 'ct', 'db', 'deb', 'dll', 'dmg', 'doc', 'docx', 'dwg', 'e', 'enc', 'epub', 'exe', 'flac', 'gho', 'hwt',
		'imazingapp', 'img', 'ipa', 'ipa', 'iso', 'it', 'jar', 'ke', 'lolgezi', 'lua', 'mobi', 'mobileconfig', 'mp3',
		'osk', 'osz', 'pdf', 'ppt', 'pptx', 'rar', 'rp', 'rplib', 'rpm', 'ssf', 'tar', 'ttc', 'ttf', 'txf', 'txt',
		'w3x', 'xapk', 'xls', 'xlsx', 'xmind', 'xpa', 'z', 'zip'
	];

	const prefix = 'lanzou_';

	private static $cookie;
	private static $admin_pass;  // 管理员密码
	private static $default_pwd; // 文件夹默认密码，等于此密码即表示无密码（2-12位数）

	private $id;   // 文件id
	private $pwd;  // 文件夹密码

	private $redis;

	/**
	 * Lanzou 初始化.
	 *
	 * @param  string  $id   文件id
	 * @param  string  $pwd  密码
	 */
	public function __construct ($id = '-1', $pwd = '') {
		$this->id  = empty($id) ? '-1' : $id;
		$this->pwd = trim($pwd);

		// 若不使用Redis缓存可注释掉下面一行
		$this->redis = Redis::prefix(self::prefix);
	}

	public static function config ($conf = []) {
		self::$admin_pass  = $conf['admin_pass'];
		self::$cookie      = $conf['cookie'];
		self::$default_pwd = $conf['default_pwd'];
	}

	/**
	 * 文件列表
	 *
	 * @param  int     $page  页码
	 * @param  string  $name  下载文件名
	 *
	 * @return array
	 */
	public function parseList ($page = 1, $name = '') {
		$admin = $this->isAdmin();
		$page  = empty($page) || !is_numeric($page) || $page < 1 ? 1 : intval($page);
		$extra = ['admin' => true === $admin];

		if ($page === 1) {
			// 获取子文件夹及父文件夹
			$folder = $this->curl()->cookie(self::$cookie)->post(self::API, [
				'task'      => 47,
				'folder_id' => $this->id
			])->obj();
			if ($folder->zt !== 1 && $folder->zt !== 2)
				return msg(1, is_string($folder->info) ? $folder->info : '解析目录失败');

			$path = array_column($folder->info, 'name', 'folderid');
			if ($this->id !== '-1' && empty($path))
				return msg(-1, '目录不存在');
			$extra['path'] = $path;

			$folders = arraySort($folder->text, 'name', SORT_NATURAL | SORT_FLAG_CASE);
		}

		if (false === $folderInfo = $this->folderInfo($this->id))
			return msg(2, '获取目录数据失败');

		$pwd = isset($folderInfo->onof) && $folderInfo->onof === '1' && isset($folderInfo->pwd) && $folderInfo->pwd !== self::$default_pwd ? $folderInfo->pwd : '';
		if ($pwd !== null && $pwd !== '') {
			if (true === $admin) {
				$extra['pwd'] = $pwd;
			} else if ($name === null || $name === '') {
				$key  = self::prefix . $this->id;
				$pwd2 = session($key);

				if ($this->pwd === '' && $pwd2 === null)
					return msg(-2, '请输入密码', $extra);

				if ($pwd !== $this->pwd && $pwd !== $pwd2)
					return msg(-3, '密码错误', $extra);

				session($key, $pwd);
			}
		}

		// 获取子文件
		$file = $this->curl()->cookie(self::$cookie)->post(self::API, [
			'task'      => 5,
			'folder_id' => $this->id,
			'pg'        => $page
		])->obj();
		if ($file->zt === 0)
			return msg(3, '解析目录失败', $extra);

		if ($name !== null && $name !== '') {
			$name = charset($name);
			foreach ($file->text as $v) {
				if ($v->name_all === $name) {
					$this->id = $v->id;

					return $this->parseUrl();
				}
			}

			return msg(4, '没有找到文件：' . $name);
		}
		$files = arraySort($file->text, 'name_all', SORT_NATURAL | SORT_FLAG_CASE);

		$data = isset($folders) ? array_merge($folders, $files) : $files;

		if (empty($data))
			return msg(5, $page > 1 ? '没有更多文件' : '空目录', $extra);

		$data = array_map(function ($v) {
			$isFolder = !isset($v->id);

			return $isFolder ? [
				'id'       => $v->fol_id,
				'name'     => $v->name,
				'isFolder' => true
			] : [
				'id'    => $v->id,
				'name'  => $v->name_all,
				'size'  => $v->size,
				'ext'   => preg_match('/\.([^.]+)$/', $v->name_all, $m) ? $m[1] : '',
				'downs' => $v->downs,
				'time'  => $v->time
			];
		}, $data);

		return msg(0, $data, $extra);
	}

	/**
	 * 解析直链（文件id）
	 *
	 * @return array
	 */
	public function parseUrl () {
		if ($this->redis !== null && $this->redis->exists($this->id)) {
			$url = $this->redis->get($this->id);
			header('Location: ' . $url);
			exit();
		}

		$obj = $this->curl()->cookie(self::$cookie)->post(self::API, [
			'task'    => 22,
			'file_id' => $this->id
		])->obj();

		if ($obj->zt !== 1 || !($info = $obj->info))
			return msg(1, is_string($obj->info) ? $obj->info : '解析分享信息失败');

		return $this->parseUrlByShareId($info->f_id, $info->onof === '1' ? $info->pwd : '', $info->is_newd . '/');
	}

	/**
	 * 解析直链（文件分享id）
	 *
	 * @param  string  $shareId  分享id
	 * @param  string  $pwd      密码
	 * @param  string  $host     域名
	 *
	 * @return array
	 */
	public function parseUrlByShareId ($shareId, $pwd = '', $host = 'https://pan.lanzoui.com/') {
		$html = $this->curl()->get($host . $shareId)->html();
		if (preg_match('/<div\s+class="off"><div\s+class="off0"><div\s+class="off1"><\/div><\/div>(.+?)<\/div>/', $html, $m))
			return msg(2, $m[1]);

		if (preg_match('/\n\s*url\s*:\s*[\'"](.+?)[\'"]/', $html, $m1)
			&& preg_match('/\n\s*data\s*:\s*[\'"](.+?&p=)[\'"]\s*\+\s*pwd\s*,/', $html, $m2)) {
			// 需要访问密码
			return $this->redirect($host . $m1[1], $m2[1] . $pwd);
		}

		if (preg_match('/<iframe\s+class="ifr2".*?src="(\/fn\?[\w]{3,}?)".*?><\/iframe>/', $html, $m)) {
			// 单文件
			$html = $this->curl()->get($host . $m[1])->html();

			if (preg_match_all('/\n\s*var\s*([a-zA-Z_]\w*)\s*=\s*[\'"](.+?)[\'"]\s*/', $html, $m) > 0
				&& preg_match('/\n\s*url\s*:\s*[\'"](.+?)[\'"]/', $html, $m1)
				&& preg_match('/\n\s*data\s*:\s*{(.+?)}/', $html, $m2)
				&& preg_match_all('/[\'"](\w+)[\'"]\s*:\s*([\'"]?(\w*)[\'"]?)/', $m2[1], $m2) > 0) {
				$vars = array_combine($m[1], $m[2]);
				$data = array_combine($m2[1], $m2[2]);
				$data = array_map(function ($v) use ($vars) {
					return isset($vars[$v]) ? $vars[$v] : trim($v, '"\'');
				}, $data);

				return $this->redirect($host . $m1[1], $data);
			}
		}

		return msg(3, '解析失败');
	}

	/**
	 * 请求直链并重定向
	 *
	 * @param             $url
	 * @param             $data
	 * @param  float|int  $expiry  过期时间，官方链接有效期是30分钟，这里最好小于30分钟
	 *
	 * @return array
	 */
	private function redirect ($url, $data, $expiry = 60 * 20) {
		$obj = $this->curl()->post($url, $data)->obj();
		if ($obj->zt === 1) {
			$url          = $obj->dom . '/file/' . $obj->url;
			$redirect_url = $this->curl()->location(false)->get($url)->all('redirect_url');
			if (filter_var($redirect_url, FILTER_VALIDATE_URL)) {
				if ($this->redis !== null) $this->redis->set($this->id, $redirect_url, $expiry);
				header('Location: ' . $redirect_url);
				exit();
			}
		}

		return msg(4, $obj->inf ?: '解析直链失败');
	}

	/**
	 * 上传文件到指定目录
	 *
	 * @param $files
	 *
	 * @return array
	 */
	public function upload ($files) {
		if (true !== $ret = $this->isAdmin())
			return $ret;

		if (empty($files) || !isset($files['file']))
			return msg(-1, '没有文件被上传');

		$file = $files['file'];
		$name = $file['name'];
		$ext  = preg_match('/\.([^.]+)$/', $name, $m) ? $m[1] : '';
		if (!in_array(strtolower($ext), self::allowedExts))
			return msg(1, '不支持的文件格式！');

		if ($file['size'] > 100 * 1024 * 1024)
			return msg(2, '文件大小不能超过100M！');

		if ($file['error'] > 0)
			return msg(3, '文件错误！');

		$temp = __DIR__ . '/.temp/';
		$dir  = $temp . date('Y-m-d') . '/';
		if (!file_exists($dir))
			mkdir($dir, 0777, true);

		$path = $dir . $name;
		if (!move_uploaded_file($file['tmp_name'], $path))
			return msg(4, '上传失败！');

		$obj = $this->curl()->cookie(self::$cookie)->upload(self::API_UPLOAD, [
			'task'        => 1,
			'folder_id'   => $this->id,
			'upload_file' => new \CURLFile($path)
		])->obj();

		@unlink($path);
		@rmdir($dir);
		@rmdir($temp);
		if ($obj->zt !== 1)
			return msg(5, is_string($obj->info) ? $obj->info : '未知错误');

		return msg(0, ['name' => $name, 'id' => $obj->text[0]->id, 'pId' => $this->id]);
	}

	/**
	 * 重命名文件夹
	 *
	 * @param          $folder_name
	 *
	 * @return array
	 */
	public function rename ($folder_name) {
		if (true !== $ret = $this->isAdmin())
			return $ret;

		if (false === $folderInfo = $this->folderInfo($this->id))
			return msg(1, '获取目录数据失败');

		$obj = $this->curl()->cookie(self::$cookie)->post(self::API, [
			'task'               => 4,
			'folder_id'          => $this->id,
			'folder_name'        => $folder_name,
			'folder_description' => isset($folderInfo->des) ? $folderInfo->des : ''
		])->obj();

		if ($obj->zt !== 1)
			return msg(2, is_string($obj->info) ? $obj->info : '修改失败');

		return msg(0, is_string($obj->info) ? $obj->info : '修改成功');
	}

	/**
	 * 新建文件夹
	 *
	 * @param  string  $name  文件夹名称
	 *
	 * @return array
	 */
	public function createFolder ($name) {
		if (true !== $ret = $this->isAdmin())
			return $ret;

		$obj = $this->curl()->cookie(self::$cookie)->post(self::API, [
			'task'        => 2,
			'parent_id'   => $this->id,
			'folder_name' => $name,
			//'folder_description' => ''
		])->obj();

		if ($obj->zt !== 1)
			return msg(1, is_string($obj->info) ? $obj->info : '创建失败');

		return msg(0, is_string($obj->info) ? $obj->info : '创建成功', ['id' => $obj->text, 'url' => true]);
	}

	/**
	 * 移动文件
	 *
	 * @param  array  $file_id  待移动的文件id
	 *
	 * @return array
	 */
	public function moveFile ($file_id) {
		if (true !== $ret = $this->isAdmin())
			return $ret;

		$error = [];
		$total = 0;
		foreach ($file_id as $id) {
			$obj = $this->curl()->cookie(self::$cookie)->post(self::API, [
				'task'      => 20,
				'folder_id' => $this->id,
				'file_id'   => $id
			])->obj();

			if ($obj->zt !== 1)
				$error[] = $id;

			$total++;
		}
		if (!empty($error))
			return msg(1, count($error) . '个文件移动失败', $total !== count($error) ? ['url' => true] : null);

		return msg(0, '移动成功', ['url' => true]);
	}

	/**
	 * 删除文件（夹）
	 *
	 * @param  array  $files  ['file_id' => [], 'folder_id' => []]
	 *
	 * @return array
	 */
	public function delete ($files = []) {
		if (true !== $ret = $this->isAdmin())
			return $ret;

		$error = [];
		$total = 0;
		foreach ($files as $key => $value) {
			if ($value === null || $value === '') continue;
			if (!is_array($value)) $value = [$value];
			foreach ($value as $v) {
				$data = [];
				if ($key === 'file_id') $data = ['task' => 6, 'file_id' => $v];
				else if ($key === 'folder_id') $data = ['task' => 3, 'folder_id' => $v];

				if (!empty($data)) {
					$obj = $this->curl()->cookie(self::$cookie)->post(self::API, $data)->obj();

					if ($obj->zt !== 1)
						$error[] = $v;

					$total++;
				}
			}
		}
		if (!empty($error))
			return msg(1, count($error) . '个文件(夹)删除失败', $total !== count($error) ? ['url' => true] : null);

		return msg(0, '删除成功', ['url' => true]);
	}

	/**
	 * 获取文件夹信息
	 *
	 * @param $folder_id
	 *
	 * @return array|false
	 */
	private function folderInfo ($folder_id) {
		$obj = $this->curl()->cookie(self::$cookie)->post(self::API, [
			'task'      => 18,
			'folder_id' => $folder_id
		])->obj();

		if ($obj->zt !== 1)
			return false;

		return $obj->info;
	}

	/**
	 * 修改密码（2-12位数）
	 *
	 * @return array
	 */
	public function setPwd () {
		if (true !== $ret = $this->isAdmin())
			return $ret;

		$obj = $this->curl()->cookie(self::$cookie)->post(self::API, [
			'task'      => 16,
			'folder_id' => $this->id,
			'shows'     => 1,
			'shownames' => $this->pwd === '' ? self::$default_pwd : $this->pwd
		])->obj();

		if ($obj->zt !== 1)
			return msg(1, is_string($obj->info) ? $obj->info : '修改失败');

		return msg(0, '修改成功');
	}

	// 管理员登录
	public function login ($pass, $expiry = 86400) {
		if (self::$admin_pass !== $pass)
			return msg(1, '管理员密码错误');

		session('admin', Authcode::encode($pass, $_SERVER['REMOTE_ADDR'], $expiry));

		return msg(0, '登陆成功', ['url' => true]);
	}

	// 管理员退出
	public function logout () {
		if (true !== $ret = $this->isAdmin())
			return $ret;

		session('admin', null);

		return msg(0, '退出成功', ['url' => true]);
	}

	// 检测是否管理员
	private function isAdmin () {
		if (null === $str = session('admin'))
			return msg(21, '未登录', ['url' => true]);

		if (!($pass = Authcode::decode($str, $_SERVER['REMOTE_ADDR'])) || $pass !== self::$admin_pass)
			return msg(22, '登录已过期，请重新登录', ['url' => true]);

		return true;
	}

	private function curl ($ua = 'pc') {
		return Curl::ua($ua)->referer('https://pan.lanzou.com/')
			->header([
				'accept'          => 'text/html,application/xhtml+xml,application/xml',
				'accept-language' => 'zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6'
			]);
	}
}