# 涟漪云

#### 介绍
蓝奏云挂载程序，自由操作蓝奏云内文件（夹），并可获取直链下载等等。  
演示：[查看演示](https://lz.ly93.cc)，讨论交流联系QQ：29397395

#### 大体功能如下

1.  浏览任意目录内文件（夹）
2.  批量移动文件
3.  批量删除文件（夹）
4.  重命名文件夹
5.  新建文件夹
6.  文件夹加密及修改
7.  文件直链（①id形式，推荐；②文件名形式）
8.  文件上传（考虑到需要服务器中转再上传，效率低，故仅仅写好了后端接口，没有集成到前端功能）


#### 软件架构
1.  PHP >= 5.6
2.  Redis (若不需要Redis，可注释掉Classes/Lanzou.php文件中的初始化函数__construct里面的：$this->redis = ...)


#### 安装教程

1.  下载源码
2.  将源码上传至你的服务器
3.  获取cookie(浏览器F12控制台执行)： 
	```javascript
    if(!/(^|\.)woozooo\.com$/i.test(document.location.host))
        throw new Error('请登录到蓝奏云控制台在执行此代码！');
    
    var copy = function (str) {
        var oInput = document.createElement('input');
        oInput.value = str;
        document.body.appendChild(oInput);
        oInput.select();
        document.execCommand("Copy");
        oInput.remove();
        alert('复制成功');
    }
    
    var regex = /(?<=^|;)\s*([^=]+)=\s*(.+?)\s*(?=;|$)/g,
        cookies = {},re;
    while(re = regex.exec(document.cookie))
        if(re[1] === 'ylogin'||re[1] === 'phpdisk_info')
            cookies[re[1]] = re[1]+'='+re[2]+';';
    
    if(!cookies.hasOwnProperty('phpdisk_info'))
        throw new Error('获取cookie失败，请确认您已登录到蓝奏云控制台！');
    
    copy(Object.values(cookies).join(' '));
	```
4.  修改配置文件(config.php)相关数据
5.  配置伪静态（Nginx，其它环境伪静态自己参照Nginx编写）：
	```nginx
	location / {
		if (!-e $request_filename) {
			rewrite ^/(d)/([a-zA-Z0-9]+)(\.[\w]+)?$ /api.php?c=$1&id=$2 last;
			rewrite ^/([a-z0-9]+)(\.[\w]+|/([^/]+))?$ /api.php?id=$1&name=$3 last;
		}
	}
	```


#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx